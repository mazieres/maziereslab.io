<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>Gender Bias in Movies</title>

    <link rel="stylesheet" href="css/bulma.css" />
    <link rel="icon" href="../img/favicon.png" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/css/splide.min.css">
	
	<script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@latest/dist/js/splide.min.js"></script>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@mazieres" />
    <meta name="twitter:creator" content="@mazieres" />  
    <meta property="og:url"                content="https://mazieres.gitlab.io/gender-movies/index.html" />
    <meta property="og:type"               content="website" />
    <meta property="og:title"              content="Gender bias in popular movies" />
    <meta property="og:description"        content="Computational appraisal of gender representativeness in popular movies." />
    <meta property="og:image"              content="https://mazieres.gitlab.io/gender-movies/img/teaser_tw.jpg" />
    <meta property="og:image:width"              content="1200" />
    <meta property="og:image:height"              content="630" />

  </head>

<body>








<!-- CONTENT -->
<section class="section" style="padding-top:20px;font-size:110%">
  <div class="container">
    <div class="columns is-centered">
      <div class="column is-half">

      <h1 class="title is-1" style="padding-top:30px;font-size: 250%;text-align: center">
        Gender bias in popular movies
      </h1>
      <h1 class="title is-1" style="font-size: 225%;font-weight: 400;text-align: center">
		  A computational approach
      </h1>
      <p></p>
	  <figure class="image is-16by9">
	    <img src="img/teaser_tw.jpg">
	  </figure>
		  		<p class="is-large" style="padding-top:15px"><i>This page is also available in <a href="index-fr.html">French</a>, <a href="index-de.html">German</a> and brazilian <a href="index-br.html"> Portuguese</a>.</i></p>
				<br>
        <p class="is-large">This is an informal account of <a href="https://www.nature.com/articles/s41599-021-00815-9" target="_blank"><b>the research</b></a> published in <a href="https://www.nature.com/palcomms/" target="_blank"><i>Humanities and Social Sciences Communications</i></a> by <a href="https://mazieres.gitlab.io/" target="_blank">Antoine Mazieres</a>, <a href="https://telmomenezes.net/" target="_blank">Telmo Menezes</a> and <a href="https://camilleroth.github.io/" target="_blank">Camille Roth</a>.</p>

		<hr>


<div class="content is-medium">
	
<p>Current debates on gender bias in media stand on the shoulder of a long standing academic endeavor to <b>capture, analyze and deconstruct representations of the feminine and the masculine</b> on a variety of vehicles such as TV, books and radio. Since the 1950’s, researchers have been identifying sexual stereotypes, biases in occupational roles, body staging, marriage and rape, and established strong, stable patterns, for instance  women appearing "as dependent on men", "unintelligent", "less competitive", "more sexualized" (Linda Busby, <a href="https://doi.org/10.1111/j.1460-2466.1975.tb00646.x" target="_blank">1975</a>).</p>
 
<p>These analyses are hard work, requiring many researchers to agree on how to annotate a media, to define a common understanding of features and meanings, and, of course, to assess the content by watching, listening or reading many times over it. This enables the systematization of a deep understanding and appraisal of the representations at stake but limits the possibility to reproduce it at scale and at several points in time to capture underlying trends.</p>
 
<p>Our work explores the possibility of using artificial intelligence and machine learning algorithms to tackle such limits. On one hand it is not possible for a computer to systematically capture fine representations about concepts such as dependence, intelligence, competitiveness or sexualization, at least for now. On the other hand,  algorithms manage to perform tens of millions of simple tasks, such as counting occurrences, in no time. Therefore,</p>
 
<div class="box" style="font-size: 125%;font-weight: 600;text-align: center">
  We focused on a very simple task: Counting faces of women and men appearing in more than 3500 popular movies spanning over 3 decades.
</div>
 
<p>We selected movies for being amply shared on peer-to-peer networks and well documented on IMDb, the idea being to <b>capture works that are both characteristic of cultural representations and influential in shaping them</b>. For each movie we extracted one frame every 2 seconds and applied image analysis algorithms to detect the presence of faces and guess if those were of women or men. Below are examples of successful guesses along with illustrative mistakes. The fact that there are errors does not necessarily discard the whole protocol. What matters is to outline precisely when and how algorithms are mistaken to correct the accuracy of  observations on the whole dataset, which is comprised of more that 12 million images.</p>

<div class="splide">
	<div class="splide__track">
		<ul class="splide__list">
			<li class="splide__slide"><figure class="image is-2by1">
<img src="img/tt2217859-frame-001532.jpg">
</figure></li>
			<li class="splide__slide"><figure class="image is-2by1">
<img src="img/tt1825683-frame-001195.jpg">
</figure></li>
			<li class="splide__slide"><figure class="image is-2by1">
<img src="img/tt4540710-frame-001780.jpg">
</figure></li>
			<li class="splide__slide"><figure class="image is-2by1">
<img src="img/tt4871980-frame-001073.jpg">
</figure></li>
			<li class="splide__slide"><figure class="image is-2by1">
<img src="img/tt3040964-frame-001253.jpg">
</figure></li>
		</ul>
	</div>
</div>
 
 
<h3 class="title is-3">Ratio of female faces and its evolution</h3>
 
<p><b>On average, over the whole dataset, only 34.52% of faces displayed in a movie are detected as female</b>. To illustrate informally what this ratio means in practice, here are a few examples of how some top grossing movies are represented. First, among movies with a high percentage of male faces (ratio of female faces < 25 %) we find movies such as <i>Pirates of the Caribbean</i> (2007), <i>Star Wars</i> (2005), <i>Matrix</i> (2003), <i>Independence Day</i> (1996) or <i>Forest Gump</i> (1994), all with a ratio of around 23%. Movies such as <i>The Hunger Games</i> (2014), <i>Jurassic World</i> (2015), <i>Rogue One</i> (2016) and <i>Gravity</i> (2013) lie around a female-male parity, with a ratio of between 45% and 55%. Lastly, the movie with the highest ratio of female faces (68%) is <i>Bad Moms</i> (2016), closely followed by movies such as <i>Sisters</i> (2015), <i>Life of the Party</i> (2018) and <i>Cake</i> (2014).</p>
 
<p>How is this ratio <b>evolving</b> with time? We temporally split our dataset into 4 chunks comprised of an equal number of movies. We observed a <b>significant increase of the number of women</b> faces. From 1985 to 1998, this ratio is of 27% and reaches a point <b>closer to a female-male parity in the most recent period</b>, from 2014 to 2019, with a ratio of 44.9%. Also, the diversity of situations (formally, the variance of this  ratio) increases. That means that <b>films produced recently tend to delve into a more diverse range of on-screen women-men shares</b>.</p>

<a href="img/dist_evol_main.jpg" target="_blank"><img src="img/dist_evol_main.jpg" style="padding-top:30px;padding-bottom:30px;"></a>


<p>Caution is required in the interpretation of these results. Bottom line is: we measured the number of faces which is a good proxy of physical on-screen presence. We are dealing with over/under-representations, and not at all with how women or men are actually represented. <b>There is much tedious research to be done before leaping from the representativeness we assessed to the actual representations of women and men in movies</b>. There may be strong incentives for production companies to foster more female presence in movies, but this effort can easily end in a mere “purplewashing” re-enacting conservative stigmatization of gender.</p>
 
<h3 class="title is-3">Other results</h3>
 
<p>The ratio of female faces significantly <b>varies from one movie genre to the other</b>. Crime movies are the ones displaying the least number of female faces (31.3%) while Horror movies display the most with a ratio at 37.1% followed closely by Romances and Comedies. We used this variation to <b>compare our metric</b> with another famous feature called the <b>Bechdel Test</b> which a film passes if at least two named women talk to each other about something besides a man. It appears that the two metrics highly correlate in their variations from one genre to another, hinting at the hypothesis that counting faces might be a relevant proxy for more semantic features.</p>
 
<a href="img/bechdel_ratio_genre_bw.jpg" target="_blank"><img src="img/bechdel_ratio_genre_bw.jpg" style="padding-top:30px;padding-bottom:30px;"></a>
 
<p>Also, we looked into IMDb’s metadata such as budget, gross and rating value, count and demographics. Does “more women in a movie” correlate with more or less money invested in or made by a movie? Is the value of the rating of a movie on IMDb by women or men indicative of the on-screen share of women? Is the number of raters as well? It appears that despite interesting patterns there is no strong correlation (positive or negative) between these features and the ratio of female faces in a movie, except for one: the ratio of female raters. This means that <b>while the value women give to a movie does not illustrate well the number of women in a movie, the share of women among raters does</b>.</p>
 
<h3 class="title is-3">Last words and see also</h3>

<p><i>Et Voilà!</i> We insisted in limiting our claims to what we know – the thorough use of computational methods in social sciences – and not jumping to hazardous interpretations regarding gender, the studies of which we are interested in but not expert of. The <a href="https://www.nature.com/articles/s41599-021-00815-9" target="_blank">academic version of this article</a> gives more details about our research protocol, how we handled the algorithms errors, minor results on possible gender bias in the <i>mise-en-scène</i> and <i>mise-en-cadre</i> of characters, and much more. Yet, if any question is left unanswered, please feel free to contact us by <a href="mailto:antoine.mazieres@gmail.com" target="_blank">email</a> or <a href="https://twitter.com/mazieres" target="_blank">Twitter</a>.</p>

<p>You can <b>download our dataset <a href="https://api.nakala.fr/data/10.34847/nkl.543czc59/422e51f7be97a0c840a2fe8b92b6475c64d8d364" target="_blank">here</a></b>.</p>
 
<p>If you would like to know more about the topics mentioned in this article, here are a few tracks to follow:</p>
<ul>
<li>On gender in media, Linda Busby (<a href="https://doi.org/10.1111/j.1460-2466.1975.tb00646.x" target="_blank">1975</a>) and Rena Rudy et al. (<a href="https://link.springer.com/content/pdf/10.1007/s11199-010-9807-1.pdf" target="_blank">2010</a>) are great places to start with.</li>
<li>The work of Tanaya Guha et al. (<a href="https://doi.org/10.1145/2818346.2820778" target="_blank">2015</a>, <a href="https://doi.org/10.1109/JPROC.2020.3047978" target="_blank">2021</a>) has a lot of similarities with ours, with better algorithms but significantly less data (no temporal analysis, yet).</li>
<li>Some activist endeavors to monitor women’s presence in media are the <a href="https://seejane.org/research-informs-empowers/data/" target="_blank">Geena Davis Institute</a> (which uses Guha’s method) and the yearly <a href="https://www.glaad.org/whereweareontv20" target="_blank">GLAAD report</a> which provide many figures and analysis.</li>
<li>The work of Joy Buolamwini and Timnit Gebru (<a href="https://proceedings.mlr.press/v81/buolamwini18a.html?mod=article_inline" target="_blank">2018</a>) and Kate Crawford and Trevor Paglen (<a href="https://excavating.ai/" target="_blank">2019</a>) provide detailed criticism about the making and performance of image analysis algorithms.</li>
<li>For a bird’s view on the usage of computational image analysis in social sciences, Taylor Arnold and Lauren Tilton (<a href="https://doi.org/10.1093/llc/fqz013" target="_blank">2019</a>) might be of your interest.</li>
</ul>






</div>

      </div>
    </div>
  </div>
</section>

<script>
	new Splide( '.splide' ).mount();
</script>

</body>
</html>